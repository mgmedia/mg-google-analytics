<?php
/**
 * Plugin Name: MG Google Analytics
 * Author: MabaGroup Multimedia UG
 * Author URI: https://maba-group.com/
 * Version: 1.1.1
 * Text Domain: mg-analytics-code
 * @author MabaGroup Multimedia UG
 */

function mg_register_analytics_setting()
{
    register_setting('general',
        'mg-analytics-code',
        [
            'type'              => 'string',
            'description'       => __('Google Analytics Tracking-Code', 'mg-analytics-code'),
            'sanitize_callback' => 'sanitize_text_field',
        ]
    );

    add_settings_field(
        'mg-analytics-code',
        __('Google Analytics Tracking-Code', 'mg-analytics-code'),
        'mg_output_field_value',
        'general', 'default',
        [
            'label_for' => __('Google Analytics Tracking-Code', 'mg-analytics-code')
        ]
    );
}

function mg_output_field_value()
{
    printf(
        '<input name="mg-analytics-code" value="%s" class="regular-text code" type="text">',
        esc_html__(get_option('mg-analytics-code'))
    );
}

add_action('admin_init', 'mg_register_analytics_setting');

function mg_fire_analytics_head()
{
    $analyticsCode = get_option("mg-analytics-code");

    if (false !== $analyticsCode && ! empty(trim($analyticsCode))) {
        ?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=<?php esc_attr_e($analyticsCode) ?>"></script>
        <script>
            var gaProperty = '<?php echo esc_js($analyticsCode) ?>';
            var disableStr = 'ga-disable-' + gaProperty;
            if (document.cookie.indexOf(disableStr + '=true') > -1) {
                window[disableStr] = true;
            }
            
            function gaOptout() {
                document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
                window[disableStr] = true;
                alert('Das Tracking durch Google Analytics wurde in Ihrem Browser für diese Website deaktiviert.');
            }
        
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());
            gtag('config', '<?php echo esc_js($analyticsCode) ?>', { 'anonymize_ip': true });
</script>
        </script>
        <?php
    }
}

add_action('wp_head', 'mg_fire_analytics_head');